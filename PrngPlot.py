import os
import binascii
from random import SystemRandom as rand
import generic_plot

"""Generate a plot of [file_size] bytes of a Cryptographically Secure Pseudo-Random Number Generator"""
file_size = 1347


def getRandomFreq(count):
    rng = rand()

    # We will use decimal 0-255(0x00-0xFF) as our character set
    charset = [str(item) for item in list(range(0,257))]

    print(charset)

    # Make random bytes
    seed = int(binascii.b2a_hex(os.urandom(1)).decode(), 16)
    print(seed)
    rng.seed(seed)


    def randBytes(num):
        raw_bytes = binascii.b2a_hex(os.urandom(num)).decode()
        #bytes = [int(raw_bytes[i:i+2], 16) for i in range(0, len(raw_bytes), 2)]
        bytes = []
        for i in range(0,num):
            bytes.append(rng.randint(0, 256))
        return bytes



    freq_dict = {}

    # Initialize dictionary
    for char in charset:
        freq_dict[char] = 0

    # We will generate 18435 bytes to plot
    ciphertext = randBytes(count)

    # Count frequencies
    for byte in ciphertext:
        freq_dict[str(byte)] = (freq_dict[str(byte)]+1)
    return freq_dict


# Plot it
freq_dict = getRandomFreq(file_size)
print(freq_dict)
generic_plot.initplot(freq_dict, label="RNG output for "+str(file_size) + " bytes", sorting=True)