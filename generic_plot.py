import matplotlib.pyplot as plt
from collections import OrderedDict
import pandas as pd


# Combined character frequency dictionary from all files from HStat
def initplot(in_dict, directory="", label="Key Frequency analysis of HM01 ciphertexts", sorting=False):
    """Make a nice plot of any arbitrary dictionary"""
    # Format dictionary to be friendly with matplotlib and sort it in descending order
    if sorting:
        order = OrderedDict(sorted(in_dict.items(), key=lambda x: x[1])[::-1])
        dictionary = order
    else:
        dictionary = in_dict

    # Log(1)/log(any_base) always equals zero(wont show on plot) so we add one to the value in order for it to appear.
    # There are no rules here; We are just trying to get something done :P
    val_list = list(dictionary.values())
    key_list = list(dictionary.keys())

    values = [value for value in val_list]
    keys = [key + "(" + chr(int(key)) + ")" for key in key_list]

    # Icky GUI stuff (close your eyes if you wish to remain 'clean' :P)

    def do_matplotlib_shit(in_keys, in_values):
        """Self-explanatory"""

        # Format the x-axis
        frequencies = in_values
        freq_series = pd.Series(frequencies)
        x_labels = in_keys
        plt.figure(figsize=(12, 8))
        ax = freq_series.plot(kind='bar')
        ax.set_title(label)
        ax.set_xlabel('Character')
        ax.set_xticklabels(x_labels)

        # Format the y-axis
        ax.set_ylabel('Frequency')
        plt.subplots_adjust(left=0.035, bottom=0.12, right=0.99, top=0.95,
                        wspace=0.22, hspace=0.22)
        # Show the plot
        plt.show()

    do_matplotlib_shit(keys, values)