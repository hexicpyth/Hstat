import os
import codecs
import operator


"""Execute various types of statistical analysis on arbitrary binary data (i.e ciphertexts or other files)
import and run from the plot module to produce a fancy frequency graph and display statistics in stdout.
Written by HexicPyth"""


avg_dict = {}  # Global dictionary(ick) containing either the sum of all file's character frequencies or the mean.


def formatkey(key):
    """
    Input: latin-1 encoded byte
    Output: Decimal encoded byte
    """
    return str(ord(key))


def initializeFrequencyDict():
    """Set all keys in global frequency dictionary to zero"""

    global avg_dict
    for char in list(range(256)):
        character = chr(char)
        avg_dict[formatkey(character)] = 0


def updateFrequencyDict(key, new_value, action="mean"):
    """Update the global frequency dictionary.
    action=mean -> Update according to mean(old_value, new_value)
    action=sum  -> Add the frequencies
    """
    global avg_dict
    avg_dict.update()

    if action == "mean":
        current_value = avg_dict[formatkey(key)]
        avg_dict[formatkey(key)] = (current_value+new_value)/2

    elif action == "sum":
        current_value = avg_dict[formatkey(key)]
        avg_dict[formatkey(key)] = current_value + new_value


def log(message, flag, newline=True):
    """Print a nicely-formatted message to stdout."""
    if newline:
        print("HStat ->", flag, "->", message)
    else:
        print("HStat ->", flag, "->", message, end='')


def print_separator():
    """Self-explanatory"""
    print("\n______________________________________")

def loadfiles(data_dir):
    """"Parse all files in a directory and return them in list
    Input: Path to directory
    Output: List of files"""

    log("HStat -> Loading files from " + data_dir + "...", "Info", newline=False)
    os.chdir(data_dir)
    files = []

    for item in os.listdir(data_dir):

        files.append(os.path.abspath(item).replace("\\", "/"))

    return files


def analysefiles(files):
    """Do the following types of analysis on our file(s):
    1. Find file size
    2. Find hamming weight
    3. Find zero coount
    4. Calculate Bit uniformity (100% uniform = 50% chance of zero and 50% chance of one)
    5. Key Frequency Analysis
    Prints results of 1-4 to stdout
    avg_dict is populated with #5"""

    global avg_dict
    initializeFrequencyDict()
    for file in files:
        print_separator()
        log("Analysing file: "+file, "Info")
        try:
            raw_data = open(file, "rb").read()
            data = int.from_bytes(raw_data, byteorder="big")
            encoded_data = str(raw_data.decode('latin-1'))
            data_bin = bin(data)

            file_size = len(data_bin)
            avg_hamming_weight = file_size/2
            hamming_weight = data_bin.count("1")
            zeroes = data_bin.count("0")

            # Calculate character frequency
            chars = ''.join(set(encoded_data))
            character_frequency = {}
            for char in chars:
                character_frequency[char] = encoded_data.count(char)

            # ones over zeroes compared to perfectly uniform string of this length
            bit_uniformity = round(((file_size-abs(avg_hamming_weight-hamming_weight))/file_size)*100, 2)

            log("File size: "+str(file_size), "Info")
            log("Hamming weight: "+str(hamming_weight), "Info")
            log("Zero count: "+str(zeroes), "Info")
            log("Bit Uniformity: " + str(bit_uniformity) + "%", "Info")
            print("Character Frequency: "+str(character_frequency))
            for key in character_frequency.keys():
                new_value = character_frequency[key]
                updateFrequencyDict(key, new_value, action="sum")

            print(sum(character_frequency.values()))
            print(sum(avg_dict.values()))
        except PermissionError:
            pass


def init(file, directory=False):
    """If file is set analyse a singular file.
    Else analyse all files in directory."""
    if file == "":
        data_dir=directory
        files = loadfiles(data_dir)
        analysefiles(files)
        return avg_dict
    else:
        print(file)
        file_to_analyse = os.path.abspath(file)
        file_list = []
        file_list.append(file_to_analyse)
        analysefiles(file_list)
        return avg_dict
