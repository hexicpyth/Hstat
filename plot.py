import generic_plot
import hstat
"""Plot the result of Hstat's Key Frequency Analysis on a fancy matplotlib bar-plot :)"""

path_str = "C:/Users/user/Desktop/External HM01/F1C"  # Directory to analyse if 'file' is not set

frequencies = hstat.init("C:/Users/user/Desktop/F01 Stuff.txt", directory=path_str)
generic_plot.initplot(frequencies, sorting=True)